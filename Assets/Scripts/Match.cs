﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Swift;
using System;

public enum MatchStatus {
	Matching,
	Loading,
	Overtime
}

public class MatchHelper : Port {

	public MatchHelper() {	
		OnMessage("Match", (data)=>{

			string msg = data.ReadString();

			if("Overtime" == msg) {
				status = MatchStatus.Overtime;
				callback(MatchStatus.Overtime, null);
			} else if("Matching" == msg) {
				status = MatchStatus.Matching;
				callback(MatchStatus.Matching, data.ReadInt() + "");
			} else if("Loading" == msg) {
				status = MatchStatus.Loading;
				callback (MatchStatus.Loading, data.ReadString());
			} 

		});
	}

	~MatchHelper() {
		RemoveComponent ("MatchHelper");
	}

	Action<MatchStatus, string> callback;

	public MatchStatus status;

	public void Start(Action<MatchStatus, string> callback) {

		this.callback = callback;

		var buff = GameCore.Instance.SrvConn.BeginSend ("GameServer");
		buff.Write ("Match");
		GameCore.Instance.SrvConn.End (buff);
	}
}
