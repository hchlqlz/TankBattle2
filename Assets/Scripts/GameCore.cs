﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Swift;

public class GameCore : Core {

	public static GameCore Instance { get { return gc; } }
	static GameCore gc = new GameCore();

	bool inited = false;
	public void Init()
	{
		if (inited)
			return;

		inited = true;

		// 计时模块
		var timer = new Timer();
		Add ("Timer", timer);

		// 网络模块
		var nc = new NetCore();
		Add ("NetCore", nc);
	}

	public Connection SrvConn = null;

}
