﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Swift;

public class PlayerController : Swift.Component, IFrameDrived {

	public string GameName;

    public Connection Conn;

	/// <summary>
	/// 用于控制向服务器发送消息的间隔，得等服务器处理完上次的消息，才继续发第二个消息
	/// </summary>
	bool flag = true;

	/// <summary>
	/// 发送用户操作，0-3不攻击移动，4-7攻击移动
	/// </summary>
	/// <param name="type">Type.</param>
	void Send(int type) {
		var buff = Conn.BeginRequest (GameName, (data)=>{
			flag = true;
		}, null);
		buff.Write ("Controller");
		buff.Write (type);
		Conn.End (buff);
	}

	public void OnTimeElapsed(int te) {

		if (flag == true) {
			int type = 0;

			if (Input.GetKey (KeyCode.W)) {
				type |= 1;
			} else if (Input.GetKey (KeyCode.S)) {
				type |= 1;
				type |= 2;
			} else if (Input.GetKey (KeyCode.A)) {
				type |= 1;
				type |= 4;
			} else if (Input.GetKey (KeyCode.D)) {
				type |= 1;
				type |= 6;
			}

			if (Input.GetKey (KeyCode.J)) {
				type |= 8;
			}

			if (type != 0) {
				flag = false;
				Send (type);
			}
				
		}
	}

}
