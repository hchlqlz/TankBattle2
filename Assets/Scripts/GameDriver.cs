﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDriver : MonoBehaviour {

	Factory factory;
	Factory Factory {
		get { 
			return factory;
		}
	}
		
	PlayerController PC;

	public string GameName;

	public int Chapter;
	public int MaxChapter;
	public bool Pass;
	public int[,] Num; 

	public void Init () {

		GameCore.Instance.Remove ("Factory");
		GameCore.Instance.Remove ("PC");

		factory = new Factory ();
		GameCore.Instance.Add ("Factory", factory);
		PC = new PlayerController ();
		PC.GameName = GameName;
		PC.Conn = GameCore.Instance.SrvConn;
		GameCore.Instance.Add ("PC", PC);
	}

	public void SetGame(Game game) {
		if (factory == null) {
			throw new UnityException ("工厂还没建立");
		}
		factory.Game = game;
	}

	private void Start()
	{
		GameCore.Instance.Init();
	}

	public void FixedUpdate()
	{
		var dt = Time.fixedDeltaTime;
		var dtMs = (int)(dt * 1000);
		GameCore.Instance.RunOneFrame(dtMs);
	}
}
