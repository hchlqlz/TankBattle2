﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Swift;

namespace Server {
    class ConsoleInput : Component, IFrameDrived{

        public Server Srv;

        bool running;
        List<Action> cmds = new List<Action>();

        public ConsoleInput() {
            running = true;
            new Thread (WorkThread).Start ();
        }

        public void WorkThread() {
            while(running) {

                string cmd = Console.ReadLine ();
                switch(cmd) { 
                    case "stop":
                        lock(cmds) {
                            cmds.Add (() => {
                                running = false;
                                Srv.Stop ();
                            });
                        }
                        break;
                
                }
                
            }
        }

        public void OnTimeElapsed(int te) {

            lock(cmds) {
                foreach(var c in cmds){
                    c();
                }

                cmds.Clear ();
            }
        }

    }
}
