﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Swift;
using Utils;
using TB;

namespace Server {
    class Program {
        static void Main(string[] args) {
            Server srv = new Server ();

            Timer timer = new Timer ();
            srv.Add ("Timer", timer);
            Actor.Timer = timer;
            ActorExtention.Timer = timer;
            TimerUtils.Timer = timer;

            var nc = new NetCore ();
            srv.Add ("NetCore", nc);
            nc.StartListening ("0.0.0.0", 5201);

            var gs = new GameServer ();
            gs.Timer = timer;
            srv.Add ("GameServer", gs);

            var ci = new ConsoleInput ();
            ci.Srv = srv;
            srv.Add ("ConsoleInput", ci);

            srv.Start ();
        }
    }
}
