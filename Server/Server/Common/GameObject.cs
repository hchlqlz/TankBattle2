﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Swift;

namespace OBJ {
    class GameObject {

        public GameObject(int tid, float x, float y, float size) {
            UID = UniqueID.Create ();
            this.x = x;
            this.y = y;
            this.size = size;
        }

        public int TID;
        public string UID {
            get;
            private set;
        }
        
        public bool Alive;

        float x;
        float y;
        float size;

        #region 位置相关

        public int X {
            get {
                return (int) x;
            }
            set {
                x = value;
            }
        }

        public int Y {
            get {
                return (int) y;
            }
            set {
                y = value;
            }
        }

        public float Size {
            get {
                return size;
            }
        }

        public float GetX() {
            return x;
        }

        public float GetY() {
            return y;
        }

        public float GetSX() {
            return x - size / 2;
        }

        public float GetSY() {
            return y - size / 2;
        }

        public void SetX(float x) {
            this.x = x;
        }

        public void SetY(float y) {
            this.y = y;
        }

        public void Mov(float a, float b) {
            x += a;
            y += b;
        }

        #endregion

        public GameObject Owner;
        public string Tag;
    }
}
