﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils {
    public static class CoordinateUtil {

        public const float MinX = -6f;
        public const float MaxX = 7f;
        public const float MinY = -8.5f;
        public const float MaxY = 4.5f;

        /// <summary>
        /// 精度，用于判断是否重叠等，在精度范围内的重叠不计
        /// </summary>
        public const float EPS = 0.01f;

        public static void IntToFloatX(int x, out float _x, float size = 0.5f) {
            _x = MaxX - x * 0.5f - size / 2;
        }

        public static void IntToFloatY(int y, out float _y, float size = 0.5f) {
            _y = MinY + y * 0.5f + size / 2;
        }

        /// <summary>
        /// 判断 _x，_y 点是否在 x，y 作为左下角大小为 size 的正方形内
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="size"></param>
        /// <param name="_x"></param>
        /// <param name="_y"></param>
        /// <param name="needEPS"> true 在精度范围内可行 或 false 在绝对范围可行 </param>
        /// <returns></returns>
        public static bool IsInSquare(float x, float y, float size, float _x, float _y, bool needEPS = false) {
            if(needEPS == true) {
                return _x > x + EPS && _x < x + size - EPS && _y > y + EPS && _y < y + size - EPS;
            } else {
                return _x > x && _x < x + size && _y > y && _y < y + size;
            }
        }

 
    }
}
