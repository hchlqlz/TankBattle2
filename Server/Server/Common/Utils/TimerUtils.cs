﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Swift;

namespace Utils {
    public static class TimerUtils {

        public static Timer Timer;

        public static void WaitForMsec(int ms, Action callback) { 
            object obj = new object();
            Timer.AddEvent ("" + obj.GetHashCode (), TimeUtils.Now + ms, 0, (name) => {
                callback ();
            });
        }
    }
}
