﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils;

namespace OBJ {
    public static class GameObjectExtention {

        /// <summary>
        /// 上下左右 移动的 x、y 值变化
        /// </summary>
        public const int[,] mov = new int[,] { {1, 0}, {-1, 0}, {0, -1}, {0, 1} };

        public static void Move(this Tank o, DIR dir) {
            // 算出目的地址
            int tx = o.X + mov[(int) dir, 0];
            int ty = o.Y + mov[(int) dir, 1];

            // 从 (o.X, o.Y) 移动到 (tx, ty)

        }
    }
}
