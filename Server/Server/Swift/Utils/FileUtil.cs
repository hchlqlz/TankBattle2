﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Utils {
    public class FileUtil {
        public static string ReadFile(string addr, params string[] op) {
            byte[] buf = File.ReadAllBytes (addr);
            string res = Encoding.UTF8.GetString (buf);
            for(int i = 0; i < op.Length; i++) {
                res = res.Replace (op[i], "");
            }
            return res;
        }
    }
}
