﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils;
using System.IO;
using Utils;
using Swift;

namespace Server {
    public class Map {

        public const int HEIGHT = 26;
        public const int WIDTH = 26;
        

        int[,] map = new int[26, 26];
        List<Point> lst;
        

        public void Load(int num) {

            string str = FileUtil.ReadFile("Config/MapConfig/" + num + ".txt", "\r", "\n");
        
            for(int i = 0; i < 26; i++) {
                for(int j = 0; j < 26; j++) {
                    map[i, j] = str[i * 26 + j] - '0';
                }
            }

            for(int i = 0; i < 25; i++) {
                for(int j = 0; j < 25; j++) {
                    if(3 <= map[i, j] && map[i, j] <= 5) {
                        map[i, j + 1] = 0;
                        map[i + 1, j] = 0;
                        map[i + 1, j + 1] = 0;
                    }
                }
            }

            for(int i = 0; i < 26; i++) {
                enemyType[i] = str[26 * 26 + i] - '0';
            }

            for(int i = 0; i < 26; i++) {
                withFood[i] = str[27 * 26 + i] == '0' ? false : true;

            }

            index = 0;
        }

        int index;  // 下一个敌人
        int[] enemyType = new int[26];
        bool[] withFood = new bool[26];

        public int GetEnemyNum() {
            return 26 - index;
        }

        public bool Next(out int type, out bool with) {
            type = 0;
            with = false;

            if(index == 26) {
                return false;
            }

            type = enemyType[index];
            with = withFood[index];
            index++;

            return true;
        }

        public int GetXY(int x, int y) {
            return map[x, y];
        }

        public void RandomPos(out int x, out int y) {

            x = RandomUtils.Random (0, 24);
            y = RandomUtils.Random (0, 24);
        }
    }
}
