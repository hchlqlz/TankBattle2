﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Swift;
using TB;

namespace Server {
    class Server : Core {

        public Server() {
            var cm = new CoroutineManager();
            Add ("CoroutineManager", cm);
            ActorExtention.CM = cm;
            ActorFactory.CM = cm;
        }

        public int Interval = 50;
        bool running = false;

        public void Start() {
            running = true;
            long t = TimeUtils.Now;

            Console.WriteLine ("GameServer started.");

            while(running) {
                long now = TimeUtils.Now;
                int dt = (int) (now - t);

                RunOneFrame (dt);
                t = now;

                int sleepTime = Interval > dt ? Interval - dt : 0;
                Thread.Sleep (sleepTime);
            }

            Console.WriteLine ("GameServer stopped.");
        }

        public void Stop() {
            Close ();
            running = false;
        }
    }
}
